from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .forms import SubscriberForm
from .models import Subscriber

# Create your views here.
response = {}
def index(request):
	form = SubscriberForm()
	response['form'] = form
	return render(request, 'index.html', response)

def subscribe(request):
	if request.method == "POST":
		print('Masuk POST')
		form = SubscriberForm(request.POST)
		if form.is_valid():
			print("OK")
			newuser = form.save()
		
	return HttpResponse("OK")

def checkEmailAvailability(request, email):
	email_is_empty = Subscriber.objects.filter(email=email).count() == 0
	json_data = {'isOk': email_is_empty}
	return JsonResponse(json_data)