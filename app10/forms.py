from django import forms
from .models import Subscriber

class SubscriberForm(forms.ModelForm):
	class Meta:
		model = Subscriber
		fields = ['email', 'username', 'password']
		fields_required = ['email', 'username', 'password']
		widgets = {
			'email': forms.EmailInput(attrs={'class': 
				'form-control my-2', 'placeholder': 'Enter your email', 'id': 'email'}),
			'username': forms.TextInput(attrs={'class': 
				'form-control my-2', 'placeholder': 'Enter your username', 'id': 'username'}),
			'password': forms.PasswordInput(attrs={'class': 
				'form-control my-2', 'placeholder': 'Enter your password', 'id': 'password'}),
		}